﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymentGatewaysLibrary.PayU.Classes;
using PaymentGatewaysLibrary.PayU.Services.Interface;
using OB.Log;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json.Serialization;
using static PaymentGatewaysLibrary.PayU.Enum.StatusEnum;
using static PaymentGatewaysLibrary.PayU.Enum.StateEnum;
using PaymentGatewaysLibrary.PayU.ClassesAuthorize;
using PaymentGatewaysLibrary.PayU.ClassesCharge;
using System.Net.Http.Headers;
using PaymentGatewaysLibrary.PayU.Classes.PaymentGatewaysLibrary.PayU.Classes;

namespace PaymentGatewaysLibrary.PayUGateway
{
    public class PayU : IPayU
    {
        private ILogger logger = LogsManager.CreateLogger("PayU PaymentGateway");
        private IPayUPayment _payUPayment;

        private PaymentGatewayConfig _config;
        private HeaderParametersRequest _headerParameter;

        public IPayUPayment PayUPayment { get; set; }

        /// <summary>
        /// To receive the request to HeaderParameters
        /// </summary>
        /// <returns></returns>
        public PayU(PaymentGatewayConfig config)
        {
            this._config = config;

            _headerParameter = new HeaderParametersRequest();
            _headerParameter.ApiVersion = _config.ApiVersion;
            _headerParameter.PublicKey = _config.MerchantKey;
            _headerParameter.PrivateKey = _config.ApiSignatureKey;
            _headerParameter.ApiId = _config.MerchantId;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            PayUPayment = new PayUPayment(_config);
        }

        #region Métodos Publicos

        /// <summary>
        /// To receive the request to CreateToken
        /// </summary>
        /// <param name="chargeCreateToken"></param>
        /// <returns></returns>
        public PaymentMessageResultPayU ChargeCreateToken(ChargeCreateTokenRequest chargeCreateToken)
        {

            if (PayUPayment == null)
            {
                PayUPayment = new PayUPayment(_config);
            }

            var resultMessage = new PaymentMessageResultPayU();
            var response = new ChargeCreateTokenResponse();

            if (_headerParameter.ApiId == null)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = "There is no ApiId configured or is invalid.";
                return resultMessage;
            }

            try
            {
                Task.Run(() => response = PayUPayment.ChargeCreateTokenResponse(chargeCreateToken, _headerParameter).Result).Wait();

                if (response.Errors == null)
                {

                    resultMessage.PaymentGatewayRequestXml = JsonConvert.SerializeObject(chargeCreateToken);
                    resultMessage.IsTransactionValid = false;
                    resultMessage.PaymentGatewayTransactionType = Common.Constants.AUTHORIZATION_TRANSACTION;
                    resultMessage.TransactionEnvironment = _config.IsTestEnvironment ? "TEST" : "LIVE";

                    resultMessage.PaymentGatewayTransactionStatusCode = response.State.ToString();
                    resultMessage.PaymentGatewayTransactionStatusDescription = response.State.ToString();

                    resultMessage.PaymentGatewayTransactionID = response.Token.ToString();

                    //Sucess            
                    if (response.State.Equals(State.Created.ToString(), StringComparison.InvariantCultureIgnoreCase)) //Ignorar maiuscula
                    {
                        resultMessage.IsTransactionValid = true;
                        resultMessage.PaymentGatewayTransactionType = response.TokenType;
                    }

                    //Decline
                    else
                    {
                        resultMessage.IsTransactionValid = false;
                        resultMessage.ErrorMessage = "PayU - Payment status declined, status: " + (Enum)Enum.Parse(typeof(Status), response.State.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = exceptionErros(ex);
                resultMessage.PaymentGatewayTransactionStatusCode = "-1";
            }
            finally
            {
                resultMessage.PaymentGatewayProcessorName = _config.ProcessorName;
                resultMessage.PaymentGatewayName = _config.GatewayName;
                resultMessage.PaymentGatewayTransactionDateTime = DateTime.UtcNow.ToString();
                resultMessage.PaymentGatewayResponseXml = JsonConvert.SerializeObject(response);
            }

            var auxResponse = JsonConvert.SerializeObject(resultMessage);

            return resultMessage;
        }

        /// <summary>
        /// To receive the request to CreatePayment
        /// </summary>
        /// <param name="createPayment"></param>
        /// <returns></returns>
        public PaymentMessageResultPayU CreatePayment(CreatePaymentRequest createPayment)
        {
            if (PayUPayment == null)
            {
                PayUPayment = new PayUPayment(_config);
            }

            var resultMessage = new PaymentMessageResultPayU();
            var response = new CreatePaymentResponse();

            if (_headerParameter.ApiId == null)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = "There is no ApiId configured or is invalid.";
                return resultMessage;
            }

            try
            {
                Task.Run(() => response = PayUPayment.CreatePaymentResponse(createPayment, _headerParameter).Result).Wait();

                if (response.Errors == null)
                {

                    resultMessage.PaymentGatewayRequestXml = JsonConvert.SerializeObject(createPayment);
                    resultMessage.IsTransactionValid = false;
                    resultMessage.PaymentGatewayTransactionType = Common.Constants.AUTHORIZATION_TRANSACTION;
                    resultMessage.TransactionEnvironment = _config.IsTestEnvironment ? "TEST" : "LIVE";

                    //Sucess
                    if (response.Status.Equals(Status.Initialized.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        resultMessage.IsTransactionValid = true;
                        resultMessage.PaymentAmountCaptured = response.Amount;
                        resultMessage.PaymentCurrencyCaptured = response.Currency;
                    }

                    //Decline
                    else
                    {
                        resultMessage.IsTransactionValid = false;
                        resultMessage.ErrorMessage = "PayU - Payment status declined, status: " + (Enum)Enum.Parse(typeof(Status), response.Status.ToString());
                    }

                    resultMessage.PaymentGatewayTransactionStatusCode = response.Status.ToString();
                    resultMessage.PaymentGatewayTransactionStatusDescription = response.Status.ToString();

                    resultMessage.PaymentGatewayTransactionID = response.Id.ToString();
                    //resultMessage.PaymentGatewayOrderID = order.Id.ToString();
                }
            }
            catch (Exception ex)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = exceptionErros(ex);
                resultMessage.PaymentGatewayTransactionStatusCode = "-1";
            }
            finally

            {

                resultMessage.PaymentGatewayProcessorName = _config.ProcessorName;
                resultMessage.PaymentGatewayName = _config.GatewayName;
                resultMessage.PaymentGatewayTransactionDateTime = DateTime.UtcNow.ToString();
                resultMessage.PaymentGatewayResponseXml = JsonConvert.SerializeObject(response);
            }

            var auxResponse2 = JsonConvert.SerializeObject(resultMessage);

            return resultMessage;
        }

        /// <summary>
        /// To receive the request to Authorize
        /// </summary>
        /// <param name="authorizeRequest"></param>
        /// <returns></returns>
        public PaymentMessageResultPayU Authorize(AuthorizeRequest authorizeRequest)
        {
            if (PayUPayment == null)
            {
                PayUPayment = new PayUPayment(_config);
            }

            var resultMessage = new PaymentMessageResultPayU();
            var response = new AuthorizeResponse();

            if (_headerParameter.ApiId == null)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = "There is no ApiId configured or is invalid.";
                return resultMessage;
            }

            try
            {
                var createToken = ChargeCreateToken(authorizeRequest.chargeCreateToken);

                if (createToken.IsTransactionValid)
                {
                    var createPayment = CreatePayment(authorizeRequest.createPayment);

                    if (createPayment.IsTransactionValid)
                    {
                        var authorizeRequestFinal = mapAuthorizeRequest(authorizeRequest, createToken.PaymentGatewayTransactionID, createPayment.PaymentGatewayTransactionID);

                        Task.Run(() => response = PayUPayment.AuthorizeResponse(authorizeRequestFinal, _headerParameter).Result).Wait();

                        if (response.Errors == null)
                        {
                            resultMessage.TransactionEnvironment = _config.IsTestEnvironment ? "TEST" : "LIVE";
                            resultMessage.PaymentGatewayTransactionType = response.PaymentMethod.Type;

                            resultMessage.PaymentGatewayTransactionID = createPayment.PaymentGatewayTransactionID;

                        }
                        else
                        {
                            resultMessage.IsTransactionValid = false;
                            resultMessage.ErrorMessage = MapErrors(response.Errors);
                        }
                        var auxResponse = JsonConvert.SerializeObject(response);
                    }
                    else
                    {
                        resultMessage.IsTransactionValid = false;
                        resultMessage.ErrorMessage = createPayment.ErrorMessage;
                    }
                }
                else
                {
                    resultMessage.IsTransactionValid = false;
                    resultMessage.ErrorMessage = createToken.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = exceptionErros(ex);
            }
            return resultMessage;
        }

        /// <summary>
        /// To receive the request to Capture
        /// </summary>
        /// <param name="captureRequest"></param>
        /// <returns></returns>
        public PaymentMessageResultPayU Capture(CaptureRequest captureRequest)
        {
            if (PayUPayment == null)
            {
                PayUPayment = new PayUPayment(_config);
            }

            var resultMessage = new PaymentMessageResultPayU();
            var response = new CaptureResponse();

            if (_headerParameter.ApiId == null)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = "There is no ApiId configured or is invalid.";
                return resultMessage;
            }

            try
            {
                var authorizationCreated = Authorize(captureRequest.authorize);

                if (authorizationCreated.IsTransactionValid)
                {

                    Task.Run(() => response = PayUPayment.CaptureResponse(captureRequest, _headerParameter).Result).Wait();

                    if (response.Errors == null)
                    {
                        resultMessage.TransactionEnvironment = _config.IsTestEnvironment ? "TEST" : "LIVE";
                    }
                    else
                    {
                        resultMessage.IsTransactionValid = false;
                        resultMessage.ErrorMessage = MapErrors(response.Errors);
                    }
                    var auxResp = JsonConvert.SerializeObject(response);

                }
                else
                {
                    resultMessage.IsTransactionValid = false;
                }

                var auxResponse = JsonConvert.SerializeObject(response);

            }
            catch (Exception ex)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = exceptionErros(ex);
            }

            return resultMessage;
        }

        /// <summary>
        /// To receive the request to Charge
        /// </summary>
        /// <param name="chargeRequest"></param>
        /// <returns></returns>
        public PaymentMessageResultPayU Charge(ChargeRequest chargeRequest)
        {
            if (PayUPayment == null)
            {
                PayUPayment = new PayUPayment(_config);
            }

            var resultMessage = new PaymentMessageResultPayU();
            var response = new ChargesResponse();

            if (_headerParameter.ApiId == null)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = "There is no ApiId configured or is invalid.";
                return resultMessage;
            }


            try
            {
                var createToken = ChargeCreateToken(chargeRequest.chargeCreateToken);

                if (createToken.IsTransactionValid)
                {
                    var createPayment = CreatePayment(chargeRequest.createPayment);

                    if (createPayment.IsTransactionValid)
                    {
                        var chargeRequestFinal = mapChargeRequest(chargeRequest, createToken.PaymentGatewayTransactionID, createPayment.PaymentGatewayTransactionID);

                        Task.Run(() => response = PayUPayment.ChargeResponse(chargeRequestFinal, _headerParameter).Result).Wait();

                        if (response.Errors == null)
                        {
                            resultMessage.TransactionEnvironment = _config.IsTestEnvironment ? "TEST" : "LIVE";
                            resultMessage.PaymentGatewayTransactionType = response.PaymentMethodsCustomer.Type;

                            resultMessage.PaymentGatewayTransactionID = createPayment.PaymentGatewayTransactionID;

                        }
                        else
                        {
                            resultMessage.IsTransactionValid = false;
                            resultMessage.ErrorMessage = MapErrors(response.Errors);
                        }
                        var auxResponse = JsonConvert.SerializeObject(response);
                    }
                    else
                    {
                        resultMessage.IsTransactionValid = false;
                        resultMessage.ErrorMessage = createPayment.ErrorMessage;
                    }
                }
                else
                {
                    resultMessage.IsTransactionValid = false;
                    resultMessage.ErrorMessage = createToken.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = exceptionErros(ex);
            }

            return resultMessage;
        }

        /// <summary>
        /// To receive the request to Void
        /// </summary>
        /// <param name="voidRequest"></param>
        /// <returns></returns>
        public PaymentMessageResultPayU Void(VoidRequest voidRequest)
        {
            if (PayUPayment == null)
            {
                PayUPayment = new PayUPayment(_config);
            }

            var resultMessage = new PaymentMessageResultPayU();
            var response = new VoidResponse();

            if (_headerParameter.ApiId == null)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = "There is no ApiId configured or is invalid.";
                return resultMessage;
            }

            try
            {

                Task.Run(() => response = PayUPayment.VoidResponse(voidRequest, _headerParameter).Result).Wait();

                if (response.Errors == null)
                {
                    resultMessage.TransactionEnvironment = _config.IsTestEnvironment ? "TEST" : "LIVE";
                }
                else
                {
                    resultMessage.IsTransactionValid = false;
                    resultMessage.ErrorMessage = MapErrors(response.Errors);
                }

                var auxResponse = JsonConvert.SerializeObject(response);

            }
            catch (Exception ex)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = exceptionErros(ex);
            }

            return resultMessage;
        }

        /// <summary>
        /// To receive the request to Refund
        /// </summary>
        /// <param name="refundRequest"></param>
        /// <returns></returns>
        public PaymentMessageResultPayU Refund(RefundRequest refundRequest)
        {
            if (PayUPayment == null)
            {
                PayUPayment = new PayUPayment(_config);
            }

            var resultMessage = new PaymentMessageResultPayU();
            var response = new RefundsResponse();

            if (_headerParameter.ApiId == null)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = "There is no ApiId configured or is invalid.";
                return resultMessage;
            }

            try
            {

                Task.Run(() => response = PayUPayment.RefundResponse(refundRequest, _headerParameter).Result).Wait();


                if (response.Errors == null)
                {
                    resultMessage.TransactionEnvironment = _config.IsTestEnvironment ? "TEST" : "LIVE";
                }
                else
                {
                    resultMessage.IsTransactionValid = false;
                    resultMessage.ErrorMessage = MapErrors(response.Errors);
                }

                var auxResponse = JsonConvert.SerializeObject(response);

            }
            catch (Exception ex)
            {
                resultMessage.IsTransactionValid = false;
                resultMessage.ErrorMessage = exceptionErros(ex);
            }

            return resultMessage;
        }

        #endregion

        #region Métodos Privados

        private string exceptionErros(Exception ex)
        {
            string result = string.Empty;

            if (ex.InnerException != null)
            {
                result = ex.InnerException.ToString();
            }
            else
            {
                result = ex.Message;
            }

            return result;
        }

        private string MapErrors(List<ErrorModel> errors)
        {
            string result = string.Empty;
            foreach (var item in errors)
            {
                result += item.Description + System.Environment.NewLine;
            }

            return result;
        }

        private AuthorizeRequest mapAuthorizeRequest(AuthorizeRequest objold, string createToken, string createPayment)
        {
            var objNew = new AuthorizeRequest();
            objNew.PaymentMethod = new PaymentMethodRequest();
            objNew.PaymentMethod.Token = createToken;
            objNew.PaymentMethod.CreditCardCvv = objold.PaymentMethod.CreditCardCvv;
            objNew.PaymentMethod.Type = objold.PaymentMethod.Type;
            objNew.ReconciliationId = objold.ReconciliationId;
            objNew.IdCreatePayment = createPayment;

            return objNew;
        }

        private ChargeRequest mapChargeRequest(ChargeRequest objold, string createToken, string createPayment)
        {
            var objNew = new ChargeRequest();
            objNew.PaymentMethod = new PaymentMethodRequest();
            objNew.PaymentMethod.Token = createToken;
            objNew.PaymentMethod.CreditCardCvv = objold.PaymentMethod.CreditCardCvv;
            objNew.PaymentMethod.Type = objold.PaymentMethod.Type;
            objNew.ReconciliationId = objold.ReconciliationId;
            objNew.IdCreatePayment = createPayment;

            return objNew;
        }

        #endregion
    }
}
