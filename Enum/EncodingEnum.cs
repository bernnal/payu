﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Enum
{
    public class EncodingEnum
    {
        public enum Encoding
        {
            [Description("HEX")]
            HEX = 0,
            [Description("BASE64")]
            BASE64 = 1,
        }
    }
}