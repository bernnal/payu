﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Enum
{
    public class StatusAuthorizeEnum
    {
        public enum StatusAuthrorize
        {
            [Description("Pending")]
            Pending = 0,
            [Description("Succeed")]
            Succeed = 1,
            [Description("Failed")]
            Failed = 2,
        }
    }
}