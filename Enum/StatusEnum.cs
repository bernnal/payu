﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Enum
{
    public class StatusEnum
    {
        public enum Status
        {
            [Description("Initialized")]
            Initialized = 1,
            [Description("Credited")]
            Credited = 2,
            [Description("Pending")]
            Pending = 3,
            [Description("Authorized")]
            Authorized = 4,
            [Description("Captured")]
            Captured = 5,
            [Description("Refunded")]
            Refunded = 6,
            [Description("Voided")]
            Voided = 7
        }
    }
}