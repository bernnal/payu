﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Enum
{
    public class ActionEnum
    {
        public enum Action
        {
            [Description("Authorize")]
            Authorize = 0,
            [Description("Charge")]
            Charge = 1,
            [Description("Capture")]
            Capture = 2,
            [Description("Refund")]
            Refund = 3,
            [Description("Void")]
            Void = 4,
            [Description("Update Payment")]
            UpdatePayment = 5
        }
    }
}

