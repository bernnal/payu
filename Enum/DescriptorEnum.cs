﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Enum
{
    public class DescriptorEnum
    {
        public enum Descriptor
        {
            [Description("receipt")]
            Receipt = 0,
            [Description("invoice")]
            Invoice = 1,
            [Description("bank_list")]
            BankList = 2,
        }
    }
}
