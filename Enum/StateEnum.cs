﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Enum
{
    public class StateEnum
    {
        public enum State
        {
            [Description("valid")]
            Valid = 0,
            [Description("created")]
            Created = 1,
            [Description("used")]
            Used = 2,
            [Description("assigned")]
            Assigned = 3,
        }
    }
}
