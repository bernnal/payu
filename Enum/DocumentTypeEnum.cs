﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Enum
{
    public class DocumentTypeEnum
    {
        public enum DocumentType
        {
            [Description("pdf")]
            Pdf = 0,
            [Description("html")]
            Html = 1,
            [Description("json")]
            Json = 2,
        }
    }
}
