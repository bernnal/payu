﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.ClassesAuthorize
{
    public class InstallmentsResponse
    {
        [DataMember(IsRequired = true, EmitDefaultValue = false)]
        [JsonProperty("number_of_installments")]
        public int NumberOfInstallments { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        [JsonProperty("first_payment_amount")]
        public int FirstPaymentAmount { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        [JsonProperty("remaining_payments_amount")]
        public int RemainingPaymentsAmount { get; set; }
    }
}
