﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.ClassesAuthorize
{
    public class PaymentMethodResponse
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("href")]
        public string Href { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("type")]
        public string Type { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("token")]
        public string Token { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("created")]
        public string Created { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("customer")]
        public string Customer { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("additional_details")]
        public AdditionalDetailsResponse AdditionalDetails { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("fingerprint")]
        public string FingerPrint { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("identity_document")]
        public IdentifyDocumentResponse IdentifyDocument { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("billing_address")]
        public BillingAddressResponse BillingAddress { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("holder_name")]
        public string HolderName { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("expiration_date")]
        public string ExpirationDate { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("last_4_digits")]
        public string Last4Digits { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("pass_luhn_validation")]
        public bool PassLuhnValidation { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("bin_number")]
        public string BinNumber { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("vendor")]
        public string Vendor { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("issuer")]
        public string Issuer { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("card_type")]
        public string CardType { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("level")]
        public string Level { get; set; }
    }
}
