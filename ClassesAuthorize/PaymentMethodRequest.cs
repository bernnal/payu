﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.ClassesAuthorize
{
    public class PaymentMethodRequest
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("token")]
        public string Token { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("credit_card_cvv")]
        public string CreditCardCvv { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
