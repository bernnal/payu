﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.ClassesAuthorize
{
    public class DocumentsResponse
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("descriptor")]
        public string Descriptor { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("content_type")]
        public string ContentType { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("href")]
        public string Href { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("content")]
        public string Content { get; set; }
    }
}
