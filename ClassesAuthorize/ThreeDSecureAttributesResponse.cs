﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.ClassesAuthorize
{
    public class ThreeDSecureAttributesResponse
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("encoding")]
        public string Encoding { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("xid")]
        public string xid { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("cavv")]
        public string Cavv { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("eci_flag")]
        public string EciFlag { get; set; }
    }
}
