﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.ClassesAuthorize
{
    public class CofTransactionIndicatorsRequest
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("card_entry_mode")]
        public string CardEntryMode { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("cof_consent_transaction_id")]
        public string CofConsentTransactionId { get; set; }
    }
}
