﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.ClassesCharge
{
    public class IdentityDocumentRequest
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("type")]
        public string Type { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("number")]
        public string Number { get; set; }
    }
}
