﻿using Newtonsoft.Json;
using PaymentGatewaysLibrary.PayU.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.ClassesCharge
{
    public class ChargeCreateTokenResponse : BaseResponse
    {
        public ChargeCreateTokenResponse()
        {

        }

        public ChargeCreateTokenResponse(BaseResponse baseResponse)
        {
            base.Errors = baseResponse.Errors;
            base.HttpStatusCode = baseResponse.HttpStatusCode;
        }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("pass_luhn_validation")]
        public bool PassLuhnValidation { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("bin_number")]
        public string BinNumber { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("vendor")]
        public string Vendor { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("issuer")]
        public string Issuer { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("card_type")]
        public string CardType { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("level")]
        public string Level { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("state")]
        public string State { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("token")]
        public string Token { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("created")]
        public string Created { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("type")]
        public string Type { get; set; }

    }
}
