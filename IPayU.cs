﻿using PaymentGatewaysLibrary.PayU.Classes;
using PaymentGatewaysLibrary.PayU.ClassesAuthorize;
using PaymentGatewaysLibrary.PayU.ClassesCharge;
using PaymentGatewaysLibrary.PayU.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayUGateway
{
    public interface IPayU : IPaymentGateway
    {
        /// <summary>
        /// 1 - Charge Create Token
        /// </summary>
        /// <param name="chargeCreateToken"></param>
        /// <returns></returns>
        PaymentMessageResultPayU ChargeCreateToken(ChargeCreateTokenRequest chargeCreateToken);

        ///<summary>
        ///Create Payment PayU
        ///</summary>
        ///<param name="createPayment">object request</param>
        ///<returns>PaymentMessageResult</returns>
        PaymentMessageResultPayU CreatePayment(CreatePaymentRequest createPayment);

        ///<summary>
        ///Authorize PayU
        ///</summary>
        ///<param name="authorizeRequest">object request</param>
        ///<returns>PaymentMessageResult</returns>
        PaymentMessageResultPayU Authorize(AuthorizeRequest authorizeRequest);

        ///<summary>
        ///Capture PayU
        ///</summary>
        ///<param name="captureRequest">object request</param>
        ///<returns>PaymentMessageResult</returns>
        PaymentMessageResultPayU Capture(CaptureRequest captureRequest);

        ///<summary>
        ///Charge PayU
        ///</summary>
        ///<param name="chargeRequest">object request</param>
        ///<returns>PaymentMessageResult</returns>
        PaymentMessageResultPayU Charge(ChargeRequest chargeRequest);

        ///<summary>
        ///Void PayU
        ///</summary>
        ///<param name="voidRequest">object request</param>
        ///<returns>PaymentMessageResult</returns>
        PaymentMessageResultPayU Void(VoidRequest voidRequest);

        ///<summary>
        ///Refund PayU
        ///</summary>
        ///<param name="refundRequest">object request</param>
        ///<returns>PaymentMessageResult</returns>
        PaymentMessageResultPayU Refund(RefundRequest refundRequest);

        IPayUPayment PayUPayment { get; set; }
    }
}
