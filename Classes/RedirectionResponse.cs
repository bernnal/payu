﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class RedirectionResponse
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("created")]
        public string Created { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("merchant_site_url")]
        public string MerchantSiteUrl { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
