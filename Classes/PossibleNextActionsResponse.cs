﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class PossibleNextActionsResponse
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("action")]
        public string Action { get; set; } //ACTION ENUM

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("href")]
        public string Href { get; set; }
    }
}