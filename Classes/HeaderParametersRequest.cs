﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class HeaderParametersRequest
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("api_version")]
        public string ApiVersion { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("x-payments-os-env")]
        public string XPaymentsOsEnv { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("app-id")]
        public string ApiId { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("public_key")]
        public string PublicKey { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("private-key")]
        public string PrivateKey { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("x-client-user-agent")]
        public string XClientUserAgent { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("x-client-ip-address")]
        public string XClientIpAddress { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("idempotency-key")]
        public string IdempotencyKey { get; set; }
    }
}
