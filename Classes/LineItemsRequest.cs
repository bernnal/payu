﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class LineItemsRequest
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("name")]
        public string Name { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("unit_price")]
        public int UnitPrice { get; set; }
    }
}