﻿using Newtonsoft.Json;
using PaymentGatewaysLibrary.PayU.ClassesAuthorize;
using PaymentGatewaysLibrary.PayU.ClassesCharge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class ChargeRequest
    {
        public ChargeCreateTokenRequest chargeCreateToken { get; set; }

        public CreatePaymentRequest createPayment { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("idCreatePayment")]
        public string IdCreatePayment { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("reconciliation_id")]
        public string ReconciliationId { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("payment_method")]
        public PaymentMethodRequest PaymentMethod { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("three_d_secure_attributes")]
        public ThreeDSecureAttributesRequest ThreeDSecureAttributes { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("installments")]
        public InstallmentsRequest Installments { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("merchant_site_url")]
        public string MerchantSiteUrl { get; set; }
    }
}
