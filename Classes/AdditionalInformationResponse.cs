﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class AdditionalInformationResponse
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string Property1 { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string Property2 { get; set; }
    }
}
