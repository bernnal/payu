﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class CreatePaymentResponse : BaseResponse
    {
        public CreatePaymentResponse()
        {

        }

        public CreatePaymentResponse(BaseResponse baseResponse)
        {
            base.Errors = baseResponse.Errors;
            base.HttpStatusCode = baseResponse.HttpStatusCode;
        }


        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("created")]
        public string Created { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("modified")]
        public string Modified { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("status")]
        public string Status { get; set; } //STATUS ENUM

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("amount")]
        public int Amount { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("additional_details")]
        public AdditionalDetailsResponse AdditionalDetails { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("statement_soft_descriptor")]
        public string StatementSoftDescriptor { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("order")]
        public OrderResponse Order { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("customer")]
        public CustomerResponse Customer { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("shipping_address")]
        public ShippingAddressResponse ShippingAddress { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("billing_address")]
        public BillingAddressResponse BillingAddress { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("payment_method")]
        public PaymentMethodResponse PaymentMethod { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("related_resources")]
        public RelatedResourcesResponse RelatedResources { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("possible_next_actions")]
        public List<PossibleNextActionsResponse> PossibleNextActions { get; set; }

    }
}
