﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class RefundRequest
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("idCreatePayment")]
        public string IdCreatePayment { get; set; }
    }
}
