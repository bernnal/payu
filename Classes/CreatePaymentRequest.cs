﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class CreatePaymentRequest
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("amount")]
        public int Amount { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("additional_details")]
        public AdditionalDetailsRequest AdditionalDetails { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("statement_soft_descriptor")]
        public string StatementSoftDescriptor { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("order")]
        public OrderRequest Order { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("customer_id")]
        public string CustomerId { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("shipping_address")]
        public ShippingAddressRequest ShippingAddress { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("billing_address")]
        public BillingAddressRequest BillingAddress { get; set; }
    }
}
