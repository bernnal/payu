﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class OrderRequest
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("additional_details")]
        public AdditionalDetaislOrderRequest AdditionalDetaislOrder { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("line_items")]
        public List<LineItemsRequest> LineItems { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("tax_amount")]
        public int TaxAmount { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("tax_percentage")]
        public double TaxPercentage { get; set; }
    }
}
