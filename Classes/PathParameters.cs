﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class PathParameters
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("payment_id")]
        public string PaymentId { get; set; }
    }
}
