﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class ErrorModel
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("category")]
        public string Category { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("description")]
        public string Description { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("more_info")]
        public string MoreInfo { get; set; }
    }
}
