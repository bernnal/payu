﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class BaseResponse
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public HttpStatusCode? HttpStatusCode { get; set; }

        public List<ErrorModel> Errors { get; set; }

        public void AddError(ErrorModel error)
        {
            if (Errors == null)
                Errors = new List<ErrorModel>();

            Errors.Add(error);
        }
    }
}
