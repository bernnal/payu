﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class PaymentMessageResultPayU
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public bool IsTransactionValid { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string ErrorMessage { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayRequestXml { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayTransactionType { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string TransactionEnvironment { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public decimal PaymentAmountCaptured { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentCurrencyCaptured { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayTransactionStatusCode { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayTransactionStatusDescription { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayTransactionID { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayCreditCardCvv { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayOrderID { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayProcessorName { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayName { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayTransactionDateTime { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string PaymentGatewayResponseXml { get; set; }

    }
}
