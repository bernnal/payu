﻿using Newtonsoft.Json;
using PaymentGatewaysLibrary.PayU.Classes.PaymentGatewaysLibrary.PayU.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class RelatedResourcesResponse
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("authorizations")]
        public List<AuthorizationsResponse> Authorizations { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("charges")]
        public List<ChargesResponse> Charges { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("voids")]
        public List<VoidResponse> Void { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("redirections")]
        public List<RedirectionsResponse> Redirections { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("captures")]
        public List<CaptureResponse> Capture { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("refunds")]
        public List<RefundsResponse> Refunds { get; set; }
    }
}