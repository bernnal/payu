﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class ProviderDataResponse
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("provider_name")]
        public string ProviderName { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("response_code")]
        public string ResponseCode { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("description")]
        public string Description { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("raw_response")]
        public string RawResponse { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("avs_code")]
        public string AvsCode { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("authorization_code")]
        public string AuthorizationCode { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("transaction_id")]
        public string TransactionId { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("external_id")]
        public string ExternalId { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("documents")]
        public List<DocumentsResponse> Documents { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("additional_information")]
        public AdditionalInformationResponse AdditionalInformation { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("network_transaction_id")]
        public string NetworkTransactionId { get; set; }
    }
}