﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class DocumentsResponse
    {
        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        public string Descriptor { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string ContentType { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string Href { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        public string Content { get; set; }
    }
}