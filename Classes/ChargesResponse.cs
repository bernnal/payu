﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    namespace PaymentGatewaysLibrary.PayU.Classes
    {
        public class ChargesResponse : BaseResponse
        {

            public ChargesResponse()
            {

            }

            public ChargesResponse(BaseResponse baseResponse)
            {
                base.Errors = baseResponse.Errors;
                base.HttpStatusCode = baseResponse.HttpStatusCode;
            }

            [DataMember(IsRequired = true, EmitDefaultValue = true)]
            [JsonProperty("href")]
            public string Href { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("id")]
            public string Id { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("result")]
            public ResultResponse Result { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("amount")]
            public int Amount { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("created")]
            public string Created { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("reconciliation_id")]
            public string ReconciliationId { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("payment_method")]
            public PaymentMethodsCustomerResponse PaymentMethodsCustomer { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("three_d_secure_attributes")]
            public ThreeDSecureAttributesResponse ThreeDSecureAttributes { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("installments")]
            public InstallmentsResponse Installments { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("provider_data")]
            public ProviderDataResponse ProviderData { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("redirection")]
            public RedirectionResponse Redirection { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("provider_specific_data")]
            public ProviderSpecificDataResponse ProviderSpecificData { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("originating_purchase_country")]
            public string OriginatingPurchaseCountry { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("ip_address")]
            public string IpAddress { get; set; }

            [DataMember(IsRequired = false, EmitDefaultValue = true)]
            [JsonProperty("additional_details")]
            public AdditionalDetailsResponse AdditionalDetails { get; set; }

        }
    }
}
