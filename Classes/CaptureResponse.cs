﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class CaptureResponse : BaseResponse
    {

        public CaptureResponse()
        {

        }

        public CaptureResponse(BaseResponse baseResponse)
        {
            base.Errors = baseResponse.Errors;
            base.HttpStatusCode = baseResponse.HttpStatusCode;
        }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("href")]
        public string Href { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("created")]
        public string Created { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("reconciliation_id")]
        public string ReconciliationId { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("result")]
        public ResultResponse ResultCapture { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("amount")]
        public int Amount { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("provider_data")]
        public ProviderDataResponse ProviderDataCapture { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("additional_details")]
        public AdditionalDetailsResponse AdditionalDetailsCapture { get; set; }
    }
}

