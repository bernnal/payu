﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class AdditionalDetaislCustomerResponse
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("key1")]
        public string Key1 { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("key2")]
        public string Key2 { get; set; }
    }
}
