﻿using Newtonsoft.Json;
using PaymentGatewaysLibrary.PayU.ClassesAuthorize;
using PaymentGatewaysLibrary.PayU.ClassesCharge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class CaptureRequest
    {
        public ChargeCreateTokenRequest chargeCreateToken { get; set; }

        public CreatePaymentRequest createPayment { get; set; }

        public AuthorizeRequest authorize { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("idCreatePayment")]
        public string IdCreatePayment { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("payment_method")]
        public PaymentMethodRequest PaymentMethod { get; set; }
    }
}
