﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class AdditionalDetaislOrderResponse
    {
        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("property1")]
        public string Property1 { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("property2")]
        public string Property2 { get; set; }
    }
}
