﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Classes
{
    public class VoidResponse : BaseResponse
    {

        public VoidResponse()
        {

        }

        public VoidResponse(BaseResponse baseResponse)
        {
            base.Errors = baseResponse.Errors;
            base.HttpStatusCode = baseResponse.HttpStatusCode;
        }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("href")]
        public string Href { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("created")]
        public string Created { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("provider_data")]
        public ProviderDataResponse ProviderDataVoid { get; set; }

        [DataMember(IsRequired = true, EmitDefaultValue = true)]
        [JsonProperty("result")]
        public ResultResponse ResultResponseVoid { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = true)]
        [JsonProperty("additional_details")]
        public AdditionalDetailsResponse AdditionalDetailsVoid { get; set; }

    }
}
