﻿using Newtonsoft.Json;
using PaymentGatewaysLibrary.PayU.Classes;
using PaymentGatewaysLibrary.PayU.Classes.PaymentGatewaysLibrary.PayU.Classes;
using PaymentGatewaysLibrary.PayU.ClassesAuthorize;
using PaymentGatewaysLibrary.PayU.ClassesCharge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Services.Interface
{
    public class PayUPayment: IPayUPayment
    {

        private PaymentGatewayConfig _config;

        public string PayUBaseEndpoint { get; set; }

        public PayUPayment(PaymentGatewayConfig config)
        {
            PayUBaseEndpoint = System.Configuration.ConfigurationManager.AppSettings["PayUBaseEndpoint"];

            _config = config;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

        }

        #region Chamadas API

        /// <summary>
        /// Conection to API to CreateToken
        /// </summary>
        /// <param name="chargeCreateTokenRequest"></param>
        /// <param name="headerParameters"></param>
        /// <returns></returns>
        public async Task<ChargeCreateTokenResponse> ChargeCreateTokenResponse(ChargeCreateTokenRequest chargeCreateTokenRequest, HeaderParametersRequest headerParameters)
        {

            using (var client = CreateDefaultClient(headerParameters))
            {
                var auxResponse = JsonConvert.SerializeObject(chargeCreateTokenRequest,
                    new JsonSerializerSettings
                    {
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        NullValueHandling = NullValueHandling.Ignore
                    });

                var response = await client.PostAsync(PayUBaseEndpoint + "tokens", new StringContent(auxResponse, Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                {
                    var auxResponseCT = await response.Content.ReadAsAsync<ChargeCreateTokenResponse>();
                    return auxResponseCT;
                }

                var nonSuccessResponse = await HandleNonSuccessAsync(response);
                return new ChargeCreateTokenResponse(nonSuccessResponse);
            }
        }

        /// <summary>
        /// Conection to API to CreatePayment
        /// </summary>
        /// <param name="createPaymentRequest"></param>
        /// <param name="headerParameters"></param>
        /// <returns></returns>
        public async Task<CreatePaymentResponse> CreatePaymentResponse(CreatePaymentRequest createPaymentRequest, HeaderParametersRequest headerParameters)
        {
            using (var client = CreateDefaultClientPayment(headerParameters))
            {
                var auxResponse = JsonConvert.SerializeObject(createPaymentRequest,
                    new JsonSerializerSettings
                    {
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        NullValueHandling = NullValueHandling.Ignore
                    });

                var response = await client.PostAsync(PayUBaseEndpoint + "payments", new StringContent(auxResponse, Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                {
                    var auxResponseCP = await response.Content.ReadAsAsync<CreatePaymentResponse>();
                    return auxResponseCP;
                }

                var nonSuccessResponse = await HandleNonSuccessAsync(response);

                return new CreatePaymentResponse(nonSuccessResponse);
            }
        }

        /// <summary>
        /// Conection to API to Authorize
        /// </summary>
        /// <param name="authorizeRequest"></param>
        /// <param name="headerParameters"></param>
        /// <returns></returns>
        public async Task<AuthorizeResponse> AuthorizeResponse(AuthorizeRequest authorizeRequest, HeaderParametersRequest headerParameters)
        {
            using (var client = CreateDefaultClientPayment(headerParameters))

            {
                var auxResponse = JsonConvert.SerializeObject(authorizeRequest,
                    new JsonSerializerSettings
                    {
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        NullValueHandling = NullValueHandling.Ignore
                    });

                var response = await client.PostAsync(PayUBaseEndpoint + "payments/" + authorizeRequest.IdCreatePayment + "/authorizations",
                    new StringContent(auxResponse, Encoding.UTF8, "application/json"));


                if (response.IsSuccessStatusCode)
                {
                    var auxResponseAut = await response.Content.ReadAsAsync<AuthorizeResponse>();
                    return auxResponseAut;
                }

                var nonSuccessResponse = await HandleNonSuccessAsync(response);
                return new AuthorizeResponse(nonSuccessResponse);

                var auxResponse2 = JsonConvert.SerializeObject(response);
            }
        }

        /// <summary>
        /// Conection to API to Capture
        /// </summary>
        /// <param name="captureRequest"></param>
        /// <param name="headerParameters"></param>
        /// <returns></returns>
        public async Task<CaptureResponse> CaptureResponse(CaptureRequest captureRequest, HeaderParametersRequest headerParameters)
        {
            using (var client = CreateDefaultClientPayment(headerParameters))

            {
                var auxResponse = JsonConvert.SerializeObject(captureRequest,
                    new JsonSerializerSettings
                    {
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        NullValueHandling = NullValueHandling.Ignore
                    });

                var response = await client.PostAsync(PayUBaseEndpoint + "payments/" + captureRequest.IdCreatePayment + "/captures",
                    new StringContent(auxResponse, Encoding.UTF8, "application/json"));


                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<CaptureResponse>();

                var nonSuccessResponse = await HandleNonSuccessAsync(response);
                return new CaptureResponse(nonSuccessResponse);

                var auxResponse2 = JsonConvert.SerializeObject(response);
            }
        }

        /// <summary>
        /// Conection to API to Charge
        /// </summary>
        /// <param name="chargeRequest"></param>
        /// <param name="headerParameters"></param>
        /// <returns></returns>
        public async Task<ChargesResponse> ChargeResponse(ChargeRequest chargeRequest, HeaderParametersRequest headerParameters)
        {
            using (var client = CreateDefaultClientPayment(headerParameters))

            {
                var auxResponse = JsonConvert.SerializeObject(chargeRequest,
                    new JsonSerializerSettings
                    {
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        NullValueHandling = NullValueHandling.Ignore
                    });

                var response = await client.PostAsync(PayUBaseEndpoint + "payments/" + chargeRequest.IdCreatePayment + "/charges",
                    new StringContent(auxResponse, Encoding.UTF8, "application/json"));


                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<ChargesResponse>();

                var nonSuccessResponse = await HandleNonSuccessAsync(response);
                return new ChargesResponse(nonSuccessResponse);

                var auxResponse2 = JsonConvert.SerializeObject(response);
            }
        }

        /// <summary>
        /// Conection to API to Void
        /// </summary>
        /// <param name="voidRequest"></param>
        /// <param name="headerParameters"></param>
        /// <returns></returns>
        public async Task<VoidResponse> VoidResponse(VoidRequest voidRequest, HeaderParametersRequest headerParameters)
        {
            using (var client = CreateDefaultClientPayment(headerParameters))

            {
                var auxResponse = JsonConvert.SerializeObject(voidRequest,
                    new JsonSerializerSettings
                    {
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        NullValueHandling = NullValueHandling.Ignore
                    });

                var response = await client.PostAsync(PayUBaseEndpoint + "payments/" + voidRequest.IdCreatePayment + "/voids",
                    new StringContent(auxResponse, Encoding.UTF8, "application/json"));


                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<VoidResponse>();

                var nonSuccessResponse = await HandleNonSuccessAsync(response);
                return new VoidResponse(nonSuccessResponse);

                var auxResponse2 = JsonConvert.SerializeObject(response);
            }
        }

        /// <summary>
        /// Conection to API to Refund
        /// </summary>
        /// <param name="refundRequest"></param>
        /// <param name="headerParameters"></param>
        /// <returns></returns>
        public async Task<RefundsResponse> RefundResponse(RefundRequest refundRequest, HeaderParametersRequest headerParameters)
        {
            using (var client = CreateDefaultClientPayment(headerParameters))

            {
                var auxResponse = JsonConvert.SerializeObject(refundRequest,
                    new JsonSerializerSettings
                    {
                        DefaultValueHandling = DefaultValueHandling.Ignore,
                        NullValueHandling = NullValueHandling.Ignore
                    });

                var response = await client.PostAsync(PayUBaseEndpoint + "payments/" + refundRequest.IdCreatePayment + "/refunds",
                    new StringContent(auxResponse, Encoding.UTF8, "application/json"));


                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<RefundsResponse>();

                var nonSuccessResponse = await HandleNonSuccessAsync(response);
                return new RefundsResponse(nonSuccessResponse);

                var auxResponse2 = JsonConvert.SerializeObject(response);
            }
        }


        #endregion

        /// <summary>
        /// Header parameters with Public Key
        /// </summary>
        /// <param name="headerParameters"></param>
        /// <returns></returns>
        private HttpClient CreateDefaultClient(HeaderParametersRequest headerParameters)
        {
            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            var client = new HttpClient(handler);
            client.DefaultRequestHeaders.Add("api-version", headerParameters.ApiVersion);
            client.DefaultRequestHeaders.Add("public_key", headerParameters.PublicKey);
            client.DefaultRequestHeaders.Add("app_id", headerParameters.ApiId);
            client.DefaultRequestHeaders.Add("x-payments-os-env", _config.IsTestEnvironment ? "test" : "env");

            return client;
        }

        /// <summary>
        /// Header parameters with Private Key
        /// </summary>
        /// <param name="headerParameters"></param>
        /// <returns></returns>
        private HttpClient CreateDefaultClientPayment(HeaderParametersRequest headerParameters)
        {
            HttpClientHandler handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            var client = new HttpClient(handler);
            client.DefaultRequestHeaders.Add("api-version", headerParameters.ApiVersion);
            client.DefaultRequestHeaders.Add("private-key", headerParameters.PrivateKey);
            client.DefaultRequestHeaders.Add("app_id", headerParameters.ApiId);
            client.DefaultRequestHeaders.Add("x-payments-os-env", _config.IsTestEnvironment ? "test" : "env");

            return client;
        }

        /// <summary>
        /// Errors handling
        /// </summary>
        /// <returns></returns>
        private async Task<BaseResponse> HandleNonSuccessAsync(HttpResponseMessage response)
        {
            var errorResponse = new BaseResponse { HttpStatusCode = response.StatusCode };

            var contentResult = await response.Content.ReadAsStringAsync();

            if (string.IsNullOrWhiteSpace(contentResult))
            {
                var error = new ErrorModel { Description = "Error Message" };
                errorResponse.AddError(error);
                return errorResponse;
            }

            var erros = JsonConvert.DeserializeObject<ErrorModel>(contentResult);
            errorResponse.Errors.Add(erros);
            return errorResponse;
        }
    }
}
