﻿using PaymentGatewaysLibrary.PayU.Classes;
using PaymentGatewaysLibrary.PayU.Classes.PaymentGatewaysLibrary.PayU.Classes;
using PaymentGatewaysLibrary.PayU.ClassesAuthorize;
using PaymentGatewaysLibrary.PayU.ClassesCharge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGatewaysLibrary.PayU.Services.Interface
{
    public interface IPayUPayment
    {
        Task<ChargeCreateTokenResponse> ChargeCreateTokenResponse(ChargeCreateTokenRequest chargeCreateTokenRequest, HeaderParametersRequest headerParameters);
        Task<CreatePaymentResponse> CreatePaymentResponse(CreatePaymentRequest createPaymentRequest, HeaderParametersRequest headerParameters);
        Task<AuthorizeResponse> AuthorizeResponse(AuthorizeRequest authorizeRequest, HeaderParametersRequest headerParameters);
        Task<CaptureResponse> CaptureResponse(CaptureRequest captureRequest, HeaderParametersRequest headerParameters);
        Task<ChargesResponse> ChargeResponse(ChargeRequest chargeRequest, HeaderParametersRequest headerParameters);
        Task<VoidResponse> VoidResponse(VoidRequest voidRequest, HeaderParametersRequest headerParameters);
        Task<RefundsResponse> RefundResponse(RefundRequest refundRequest, HeaderParametersRequest headerParameters);
    }
}
